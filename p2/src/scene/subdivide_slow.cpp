#include "scene/mesh.hpp"
#include <math.h>       /* cos */

#define CAPACITY_GROW 1  //how many times of the current needed capacity

namespace _462 {

struct MeshEdge
{
    // index of the triangles that share this edge
    unsigned int tri_index1; //cannot be null, there must be a triangle that makes the edge built in this MeshEdgeList(edges)
    unsigned int tri_index2; //default: -1, used to distingushed the boundary edge
    //index of vertices that form this edge
    unsigned int vert_index1;
    unsigned int vert_index2;
    // the new vertex on this edge
    unsigned int new_vert_index;
};

bool foundEdgeInTriangle(MeshTriangle& tri, MeshEdge& edge);

bool Mesh::subdivide()
{
    /*
      You should implement loop subdivision here.

      Triangles are stored in an std::vector<MeshTriangle> in 'triangles'.
      Vertices are stored in an std::vector<MeshVertex> in 'vertices'.

      Check mesh.hpp for the Mesh class definition.
     */
	std::cout << "execute subdivision.\n";
	typedef std::vector< MeshEdge > MeshEdgeList;
	int org_tri_size = triangles.size();
	int org_vert_size = vertices.size();
	MeshEdgeList edges;
	
	std::vector< int > *neighborVert;
	neighborVert = new std::vector< int >[org_vert_size]; 
	
	//get enough space to prevent reacclocation all the time
	edges.reserve(org_vert_size);
	if(triangles.capacity() < org_tri_size * 4){
		triangles.reserve(org_tri_size * 4 * CAPACITY_GROW);
		triangles.resize(triangles.capacity());
	}
	if(vertices.capacity() < org_vert_size * 4){
		vertices.reserve(org_vert_size * 4 * CAPACITY_GROW);
	}
	
	std::cout << triangles.size()<<"triangles, in vector capacity "<<triangles.capacity()<<".\n";
	std::cout << vertices.size()<<"vertices, in vector capacity "<<vertices.capacity()<<".\n";
	//for(int i = 0; i<org_tri_size; i++){
	for(int i = org_tri_size-1; i>=0; i--){  //go through triangles backwards.
		std::vector< int > sharedEdges;
		int smallTriangleVertices[3];

		for(int j = 0; j<edges.size(); j++){
			if(foundEdgeInTriangle(triangles[i], edges[j])){ // this triangle share the same edge
				sharedEdges.push_back(j);	
			}
		}
		for(int j = 0; j<3; j++){  //go through each edge in this triangle
			int k;
			for(k = 0; k<sharedEdges.size(); k++){
				if(edges[sharedEdges[k]].vert_index1 == triangles[i].vertices[(j+1)%3]){ 
					if(edges[sharedEdges[k]].vert_index2 == triangles[i].vertices[(j+2)%3])
						break;
				}else if (edges[sharedEdges[k]].vert_index1 == triangles[i].vertices[(j+2)%3])
					if(edges[sharedEdges[k]].vert_index2 == triangles[i].vertices[(j+1)%3]) 
						break;
			}
			if(k==sharedEdges.size()){ // this edge is new
				//create new vertex on this edge, and push it in edges vector	
				Vector3 newV( vertices[triangles[i].vertices[j]].position*(1.0f/8.0f) 
					    + vertices[triangles[i].vertices[(j+1)%3]].position*(3.0f/8.0f) 
					    + vertices[triangles[i].vertices[(j+2)%3]].position*(3.0f/8.0f));
				Vector3 newVn( vertices[triangles[i].vertices[j]].normal*(1.0f/8.0f) 
					     + vertices[triangles[i].vertices[(j+1)%3]].normal*(3.0f/8.0f) 
					     + vertices[triangles[i].vertices[(j+2)%3]].normal*(3.0f/8.0f));
				MeshVertex tempMV;
				tempMV.position = newV;
				tempMV.normal = newVn;
				vertices.push_back(tempMV);   
				MeshEdge tempE;
				tempE.tri_index1 = i;
				tempE.tri_index2 = -1;
				tempE.vert_index1 = triangles[i].vertices[(j+1)%3];
				tempE.vert_index2 = triangles[i].vertices[(j+2)%3];
				tempE.new_vert_index = vertices.size()-1;
				edges.push_back(tempE);
				neighborVert[tempE.vert_index1].push_back(tempE.vert_index2);
				neighborVert[tempE.vert_index2].push_back(tempE.vert_index1);
				smallTriangleVertices[j] = vertices.size()-1;
			}else{ // this edge is not new
				//adjust the vetex position on this edge by adding the last 1/8 vertex weight
				vertices[edges[sharedEdges[k]].new_vert_index].position += vertices[triangles[i].vertices[j]].position*(1.0f/8.0f);
				vertices[edges[sharedEdges[k]].new_vert_index].normal += vertices[triangles[i].vertices[j]].normal*(1.0f/8.0f);
				//fill out the MeshEdgeList edges' second triangle index
				edges[sharedEdges[k]].tri_index2 = i; 
				smallTriangleVertices[j] =  edges[sharedEdges[k]].new_vert_index;
			}
		}
		//build the four new triangles
		int offset = 4*i;
		if(i!=0){
			for(int j = 0; j<3; j++){
				MeshTriangle tempMT;
				tempMT.vertices[0] = triangles[i].vertices[j];
				tempMT.vertices[1] = smallTriangleVertices[(j+1)%3];
				tempMT.vertices[2] = smallTriangleVertices[(j+2)%3];
				triangles[offset+j] = tempMT;
			}
			MeshTriangle tempMT;
			tempMT.vertices[0] = smallTriangleVertices[0];
			tempMT.vertices[1] = smallTriangleVertices[1];
			tempMT.vertices[2] = smallTriangleVertices[2];
			triangles[offset+3] = tempMT;
		}else{
			MeshTriangle firstTriangle = triangles[0];
			for(int j = 0; j<3; j++){
				MeshTriangle tempMT;
				tempMT.vertices[0] = firstTriangle.vertices[j];
				tempMT.vertices[1] = smallTriangleVertices[(j+1)%3];
				tempMT.vertices[2] = smallTriangleVertices[(j+2)%3];
				triangles[offset+j] = tempMT;
			}
			MeshTriangle tempMT;
			tempMT.vertices[0] = smallTriangleVertices[0];
			tempMT.vertices[1] = smallTriangleVertices[1];
			tempMT.vertices[2] = smallTriangleVertices[2];
			triangles[offset+3] = tempMT;
			
		}
		
	}
	// calculate new position for each old vertices
	for(int i = 0; i<org_vert_size; i++){
		int neighbor_count = neighborVert[i].size();
		if(neighbor_count <= 2){ 
			if(neighbor_count ==2){
				vertices[i].position *= 3.0f/4.0f;
				vertices[i].position += vertices[neighborVert[i][0]].position/8.0f;
				vertices[i].position += vertices[neighborVert[i][1]].position/8.0f;
				vertices[i].normal *= 3.0f/4.0f;
				vertices[i].normal += vertices[neighborVert[i][0]].normal/8.0f;
				vertices[i].normal += vertices[neighborVert[i][1]].normal/8.0f;
			}
			continue;
		}
		float cosine_value = cos(2*PI/neighbor_count)/4.0f;
		float beta = (5.0f/8.0f)-pow(((3.0f/8.0f)+cosine_value), 2.0);
		beta /= neighbor_count;
		Vector3 neighbor_sum = Vector3::Zero;
		for(int j = 0; j<neighbor_count; j++){
			neighbor_sum += vertices[neighborVert[i][j]].position;
		}
		vertices[i].position *= 1-(beta*neighbor_count);
		vertices[i].position += beta*neighbor_sum;
		
		neighbor_sum = Vector3::Zero;
		for(int j = 0; j<neighbor_count; j++){
			neighbor_sum += vertices[neighborVert[i][j]].normal;
		}
		vertices[i].normal *= 1-(beta*neighbor_count);
		vertices[i].normal += beta*neighbor_sum;
	}
	// adjust new boundary vertices position
	for(int i = 0; i<edges.size(); i++){
		if(edges[i].tri_index2 == -1){ //this edge is used by only one triangle and thus is boundary edge
			//update position to 1/2 * a, 1/2 * b
			vertices[edges[i].new_vert_index].position = (vertices[edges[i].vert_index1].position)/2.0f
								    +(vertices[edges[i].vert_index1].position)/2.0f;
			vertices[edges[i].new_vert_index].normal = (vertices[edges[i].vert_index1].normal)/2.0f
								  +(vertices[edges[i].vert_index1].normal)/2.0f;
		}
	}
	// adjust new boundary vertices position
	create_gl_data();
	return true;
}

bool foundEdgeInTriangle(MeshTriangle& tri, MeshEdge& edge){
	if(edge.vert_index1 == tri.vertices[0]){
		if(edge.vert_index2 == tri.vertices[1] || edge.vert_index2 == tri.vertices[2])
			return true;
	}else if(edge.vert_index1 == tri.vertices[1]){
		if(edge.vert_index2 == tri.vertices[0] || edge.vert_index2 == tri.vertices[2])
			return true;
	}else if(edge.vert_index1 == tri.vertices[2]){
		if(edge.vert_index2 == tri.vertices[0] || edge.vert_index2 == tri.vertices[1])
			return true;
	}
	return false;
}


} /* _462 */
