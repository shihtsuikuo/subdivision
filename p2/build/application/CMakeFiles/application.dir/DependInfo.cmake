# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/Ariel_K/Downloads/p2/src/application/application.cpp" "/Users/Ariel_K/Downloads/p2/build/application/CMakeFiles/application.dir/application.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/application/camera_roam.cpp" "/Users/Ariel_K/Downloads/p2/build/application/CMakeFiles/application.dir/camera_roam.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/application/imageio.cpp" "/Users/Ariel_K/Downloads/p2/build/application/CMakeFiles/application.dir/imageio.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/application/scene_loader.cpp" "/Users/Ariel_K/Downloads/p2/build/application/CMakeFiles/application.dir/scene_loader.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/Users/Ariel_K/Downloads/p2/src"
  "/usr/local/include/SDL"
  "/usr/local/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
