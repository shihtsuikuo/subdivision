# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/Ariel_K/Downloads/p2/src/scene/material.cpp" "/Users/Ariel_K/Downloads/p2/build/scene/CMakeFiles/scene.dir/material.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/scene/mesh.cpp" "/Users/Ariel_K/Downloads/p2/build/scene/CMakeFiles/scene.dir/mesh.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/scene/model.cpp" "/Users/Ariel_K/Downloads/p2/build/scene/CMakeFiles/scene.dir/model.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/scene/scene.cpp" "/Users/Ariel_K/Downloads/p2/build/scene/CMakeFiles/scene.dir/scene.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/scene/sphere.cpp" "/Users/Ariel_K/Downloads/p2/build/scene/CMakeFiles/scene.dir/sphere.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/scene/subdivide.cpp" "/Users/Ariel_K/Downloads/p2/build/scene/CMakeFiles/scene.dir/subdivide.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/scene/triangle.cpp" "/Users/Ariel_K/Downloads/p2/build/scene/CMakeFiles/scene.dir/triangle.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/Users/Ariel_K/Downloads/p2/src"
  "/usr/local/include/SDL"
  "/usr/local/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
