# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/Ariel_K/Downloads/p2/src/math/camera.cpp" "/Users/Ariel_K/Downloads/p2/build/math/CMakeFiles/math.dir/camera.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/math/color.cpp" "/Users/Ariel_K/Downloads/p2/build/math/CMakeFiles/math.dir/color.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/math/math.cpp" "/Users/Ariel_K/Downloads/p2/build/math/CMakeFiles/math.dir/math.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/math/matrix.cpp" "/Users/Ariel_K/Downloads/p2/build/math/CMakeFiles/math.dir/matrix.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/math/quaternion.cpp" "/Users/Ariel_K/Downloads/p2/build/math/CMakeFiles/math.dir/quaternion.cpp.o"
  "/Users/Ariel_K/Downloads/p2/src/math/vector.cpp" "/Users/Ariel_K/Downloads/p2/build/math/CMakeFiles/math.dir/vector.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/Users/Ariel_K/Downloads/p2/src"
  "/usr/local/include/SDL"
  "/usr/local/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
