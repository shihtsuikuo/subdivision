// Declare any additional variables here. You may need some uniform variables.
uniform samplerCube cubemap;

varying vec3 norm;
varying vec3 cam_dir;
varying vec3 color;

void main(void)
{
	// Your shader code here.
	// Note that this shader won't compile since gl_FragColor is never set.
	// Defining The Material Colors
        
        vec3 ref = reflect(cam_dir, normalize(norm));
        vec4 color = textureCube(cubemap, ref);
	
	gl_FragColor = color;
}
